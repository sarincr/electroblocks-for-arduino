ElectroBlocks is a "drag and drop" app for programming the Arduino. You use this with Serial Monitor, Bluetooth, NeoPixels, LED Matrix, IR Remotes and much more. It also has a debug block which will pause the program and print out all the variables. You can also save and see the code view as well. 

Downloads

Windows: https://github.com/phptuts/ElectroBlocks/releases/download/v2.0.7/ElectroBlocks-Setup-2.0.7.exe

Mac: https://github.com/phptuts/ElectroBlocks/releases/download/v2.0.7/ElectroBlocks-2.0.7-mac.zip

Linux: https://github.com/phptuts/ElectroBlocks/releases/download/v2.0.7/ElectroBlocks_2.0.7_amd64.deb




